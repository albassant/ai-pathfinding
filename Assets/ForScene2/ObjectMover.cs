﻿using UnityEngine;
using System.Collections;

public class ObjectMover : MonoBehaviour
{
    public AnimationCurve curve;

	void Update ()
    {
        Vector3 B = Vector3.zero;
        Vector3 A = this.transform.position;

        Vector3 direction = B - A;

        float speed = 10;
        this.transform.position += direction.normalized * speed * Time.deltaTime;	
	}
}
