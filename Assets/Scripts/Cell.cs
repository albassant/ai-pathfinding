﻿using UnityEngine;
using System.Collections;

public class Cell : FieldObject
{
    public enum EState
    {
        Normal,
        Obstacle,
        PathEnd,
        Path,
    }

    private EState state = EState.Normal;

    public EState State
    {
        get { return state; }
        set
        {
            if (state == value)
                return;

            state = value;
            Color cl = Color.white;

            switch (state)
            {
                case EState.Normal: cl = Color.white; break;
                case EState.Obstacle: cl = Color.red; break;
                case EState.PathEnd: cl = Color.green; break;
                case EState.Path: cl = new Color(0.7f, 0.9f, 0.7f); break;
            }

            GetComponent<Renderer>().material.color = cl;
        }
    }

    public int CellNodeIndex;
}
