﻿ using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour
{
    public System.Action<Vector3> OnLeftClick;
    public System.Action<Vector3, bool> OnRightClick;

    protected Vector2 currentDrag;
    public float minDragLength = 5f;

    protected virtual void Update()
    {
    }

    public Vector3 World(Vector3 screen)
    {
        return Camera.main.ScreenToWorldPoint(screen);
    }
}
