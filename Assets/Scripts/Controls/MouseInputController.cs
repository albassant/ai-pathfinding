﻿using UnityEngine;
using System.Collections;

public class MouseInputController : InputController
{
    Vector3 firstClickPos;
    Vector3 secondClickPos;

    protected override void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnLeftClick(World(Input.mousePosition));
        }
        else
        {
            if (Input.GetMouseButtonDown(1))
            {
                firstClickPos = Input.mousePosition;
                OnRightClick(World(firstClickPos), false);
            }
            else if (Input.GetMouseButtonUp(1))
            {
                return;
            }
            else if (Input.GetMouseButton(1))
            {
                secondClickPos = Input.mousePosition;
                currentDrag = new Vector2(secondClickPos.x - firstClickPos.x, secondClickPos.y - firstClickPos.y);
                firstClickPos = secondClickPos;

                if (currentDrag.sqrMagnitude < minDragLength)
                    return;
 
                OnRightClick(World(secondClickPos), true);
            }
        }
    }
}
