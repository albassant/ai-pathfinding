﻿using UnityEngine;
using System.Collections;

public class FieldObject : MonoBehaviour
{
    private IntVector position;

    public IntVector Position
    {
        get { return position; }
        set
        {
            position = value;
            this.transform.position = Field.Instance.GameToWorld(position);
        }
    }

    private Vector3 size;
    public Vector3 Size
    {
        get { return size; }
        set
        {
            size = value;
            this.transform.localScale = size;
            Position = position;
        }
    }
}
