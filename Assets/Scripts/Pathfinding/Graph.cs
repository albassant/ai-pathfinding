﻿using System.Collections.Generic;

public class Graph
{
    Dictionary<int, List<Connection>> connections = new Dictionary<int, List<Connection>>();

    public List<Connection> GetConnections(int fromNode)
    {
        return connections[fromNode];
    }

    public void SetConnectionsForNode(int node, List<KeyValuePair<int, float>> connectionNodes)
    {
        List<Connection> conns = new List<Connection>();

        foreach (var connToNode in connectionNodes)
        {
            Connection conn = new Connection();
            conn.fromNode = node;
            conn.toNode = connToNode.Key;
            conn.cost = connToNode.Value;
            conns.Add(conn);
        }

        if (connections.ContainsKey(node))
            connections[node] = conns;
        else
            connections.Add(node, conns);
    }
}

public class Connection
{
    public float cost;
    public int fromNode;
    public int toNode;
}
