﻿using System.Collections.Generic;
using System.Linq;
using System;

public class Pathfinding
{
    public struct NodeRecord
    {
        public int node;
        public Connection connection;
        public float costSoFar;
        public float estimatedTotalCost;
    }

    Dictionary<int, NodeRecord> open = new Dictionary<int, NodeRecord>();
    Dictionary<int, NodeRecord> closed = new Dictionary<int, NodeRecord>();


    public LinkedList<int> FindPathAStar(Graph graph, int start, int end, Func<int, float> heuristicEstimate)
    {
        var startRecord = new NodeRecord();
        startRecord.node = start;
        startRecord.connection = null;
        startRecord.costSoFar = 0;
        startRecord.estimatedTotalCost = heuristicEstimate(start);

        open.Add(start, startRecord);

        NodeRecord endNodeRecord;
        NodeRecord current = startRecord;

        int iterations = 0;

        while (open.Count > 0)
        {
            iterations++;

            current = GetSmallestOpen();
 
            if (current.node == end)
                break;

            var connections = graph.GetConnections(current.node);

            Field.Instance.Cells[current.node / Field.Instance.Height, current.node % Field.Instance.Height].GetComponent<UnityEngine.Renderer>().material.color = UnityEngine.Color.gray;

            foreach (var connection in connections)
            {
                int endNode = connection.toNode;
                float endNodeCost = current.costSoFar + connection.cost;
                float endNodeHeuristic;

                if (closed.ContainsKey(endNode))
                {
                    endNodeRecord = closed[endNode];

                    if (endNodeRecord.costSoFar <= endNodeCost)
                        continue;

                    closed.Remove(endNode);
                    endNodeHeuristic = endNodeCost - endNodeRecord.costSoFar;
                }
                else if (open.ContainsKey(endNode))
                {
                    endNodeRecord = open[endNode];

                    if (endNodeRecord.costSoFar <= endNodeCost)
                        continue;

                    endNodeHeuristic = endNodeCost - endNodeRecord.costSoFar;
                }
                else
                {
                    endNodeRecord = new NodeRecord();
                    endNodeRecord.node = endNode;
                    endNodeHeuristic = heuristicEstimate(endNode);
                }

                endNodeRecord.costSoFar = endNodeCost;
                endNodeRecord.connection = connection;
                endNodeRecord.estimatedTotalCost = endNodeCost + endNodeHeuristic;

                if (!open.ContainsKey(endNode))
                    open.Add(endNode, endNodeRecord);
            }

            open.Remove(current.node);
            closed.Add(current.node, current);
        }

        UnityEngine.Debug.Log(iterations);

        if (current.node != end)
            return null;

        LinkedList<int> path = new LinkedList<int>();
        while (current.node != start)
        {
            path.AddFirst(current.node);
            current = closed[current.connection.fromNode];
        }

        return path;
    }

    NodeRecord GetSmallestOpen()
    {
        return open.Values.OrderByDescending(g => g.estimatedTotalCost).LastOrDefault();
    }
}

