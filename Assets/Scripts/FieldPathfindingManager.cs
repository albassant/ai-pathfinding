﻿using UnityEngine;
using System.Collections.Generic;

public class FieldPathfindingManager
{
    Graph pathFindGraph;
    Pathfinding pathFinder;
    LinkedList<int> currentPath = new LinkedList<int>();
    public LinkedList<int> Path { get { return currentPath; } }

    Cell startNode;
    Cell goalNode;

    public void TryBuildPath(Cell start, Cell end)
    {
        if (start == null || end == null)
            return;

        this.startNode = start;
        this.goalNode = end;

        pathFinder = new Pathfinding();
        UpdatePathfindGraph();
        currentPath = pathFinder.FindPathAStar(pathFindGraph, startNode.CellNodeIndex, goalNode.CellNodeIndex, HeuristicEstimate);
    }

    void UpdatePathfindGraph()
    {
        pathFindGraph = new Graph();

        foreach (var cell in Field.Instance.Cells)
        {
            if (cell.State == Cell.EState.Obstacle)
                continue;

            pathFindGraph.SetConnectionsForNode(cell.CellNodeIndex, GetNeighboursWithCosts(cell.Position));
        }
    }

    List<KeyValuePair<int, float>> GetNeighboursWithCosts(IntVector cellPos)
    {
        List<KeyValuePair<int, float>> neighbours = new List<KeyValuePair<int, float>>();

        for (int dx = -1; dx <= 1; dx++)
        {
            for (int dy = -1; dy <= 1; dy++)
            {
                if (Mathf.Abs(dx) == Mathf.Abs(dy))
                    continue;

                var neighbourPos = new IntVector(cellPos.X + dx, cellPos.Y + dy);

                if (neighbourPos.X < 0 || neighbourPos.X >= Field.Instance.Width
                    || neighbourPos.Y < 0 || neighbourPos.Y >= Field.Instance.Height)
                    continue;

                if (Field.Instance.Cells[neighbourPos.X, neighbourPos.Y].State == Cell.EState.Obstacle)
                    continue;

                neighbours.Add(new KeyValuePair<int, float>(Field.Instance.Cells[neighbourPos.X, neighbourPos.Y].CellNodeIndex, 1f));
            }
        }

        return neighbours;
    }

    float HeuristicEstimate(int nodeIndex)
    {
        //Debug.Log("Node index: " + nodeIndex + " nodeIndex / Field.Instance.Width: " + nodeIndex / Field.Instance.Width + " nodeIndex % Field.Instance.Width: " + nodeIndex % Field.Instance.Width);
        //var goalPos = Field.Instance.GameToWorld(goalNode.Position);
        //var nodePos = Field.Instance.GameToWorld(Field.Instance.Cells[nodeIndex / Field.Instance.Height, nodeIndex % Field.Instance.Height].Position);

        int nodeX = nodeIndex / Field.Instance.Height;
        int nodeY = nodeIndex % Field.Instance.Height;

        IntVector curNode = new IntVector(nodeX, nodeY);
        IntVector goal = goalNode.Position;

        return (curNode - goal).SqrMagnitude;
    }
}
