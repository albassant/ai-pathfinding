﻿using UnityEngine;
using System.Collections.Generic;

public class Field : MonoBehaviour
{
    public int Width;
    public int Height;

    public Cell CellPrefab;

    private Cell[,] cells;
    public Cell[,] Cells { get { return cells; } }

    private Vector3 fieldOffset;
    private float cellSize;

    private static Field instance;
    public static Field Instance
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType<Field>();
            return instance;
        }
    }

    private FieldPathfindingManager pathFindingManager;

    void Start()
    {
        cellSize = Mathf.Min(MainCamera.Instance.WorldDimensions.x / (float)Width, MainCamera.Instance.WorldDimensions.y / (float)Height);

        fieldOffset = (MainCamera.Instance.WorldCenter - new Vector3(Width, Height) * 0.5f * cellSize) + new Vector3(cellSize, cellSize) * 0.5f;

        GenerateCells();

        pathFindingManager = new FieldPathfindingManager();
    }

    void GenerateCells()
    {
        cells = new Cell[Width, Height];

        for (int i = 0; i < Width; i++)
            for (int j = 0; j < Height; j++)
            {
                cells[i, j] = GameObject.Instantiate<Cell>(CellPrefab);
                cells[i, j].transform.SetParent(this.transform);
                cells[i, j].transform.localScale = Vector3.one * cellSize;
                cells[i, j].Position = new IntVector(i, j);
                cells[i, j].CellNodeIndex = i * Height + j;
            }
    }

    #region Input handlers
    Cell.EState prevState;
    public void AddObstacle(Vector3 clickPos, bool isDragging)
    {
        var gamePos = WorldToGame(clickPos);
        var cell = cells[gamePos.X, gamePos.Y];

        if (cell.State != Cell.EState.Normal && cell.State != Cell.EState.Obstacle)
            return;

        if (!isDragging)
        {
            cell.State = cell.State == Cell.EState.Obstacle ? Cell.EState.Normal : Cell.EState.Obstacle;
            prevState = cell.State;
         }
        else
        {
            cell.State = prevState;
        }
    }

    List<Cell> pathPoints = new List<Cell>();
    public void SetPathPoint(Vector3 clickPos)
    {
        var gamePos = WorldToGame(clickPos);
        var curCell = cells[gamePos.X, gamePos.Y];

        if (curCell.State == Cell.EState.Obstacle)
            return;

        if (pathPoints.Count < 2)
        {
            pathPoints.Add(curCell);
        }
        else
        {
            pathPoints[0].State = pathPoints[1].State = Cell.EState.Normal;
            pathPoints.RemoveRange(0, pathPoints.Count);
            pathPoints.Add(curCell);
            ClearPath(); 
        }

        curCell.State = Cell.EState.PathEnd;

        if (pathPoints.Count == 2)
        {
            pathFindingManager.TryBuildPath(pathPoints[0], pathPoints[1]);
            DrawPath();
        }
    }

    void ClearPath()
    {
        if (pathFindingManager.Path == null)
            return;

        foreach (int nodeIndex in pathFindingManager.Path)
        {
            if (Cells[nodeIndex / Height, nodeIndex % Height].State != Cell.EState.PathEnd)
                Cells[nodeIndex / Height, nodeIndex % Height].State = Cell.EState.Normal;
        }
    }

    void DrawPath()
    {
        if (pathFindingManager.Path == null)
            return;

        foreach (int nodeIndex in pathFindingManager.Path)
        {
            if (Cells[nodeIndex / Height, nodeIndex % Height].State != Cell.EState.PathEnd)
                Cells[nodeIndex / Height, nodeIndex % Height].State = Cell.EState.Path;
        }
    }
    #endregion

    #region position conversion
    public IntVector WorldToGame(Vector3 worldPos)
    {
        Vector3 shiftedPos = (worldPos - fieldOffset) / cellSize;
        return new IntVector(Mathf.Clamp(Mathf.RoundToInt(shiftedPos.x), 0, Width - 1), Mathf.Clamp(Mathf.RoundToInt(shiftedPos.y), 0, Height - 1));
    }

    public Vector3 GameToWorld(IntVector gamePos)
    {
        var shiftedPos = new Vector3(gamePos.X * cellSize, gamePos.Y * cellSize) + fieldOffset;
        return shiftedPos;
    }
    #endregion
}
