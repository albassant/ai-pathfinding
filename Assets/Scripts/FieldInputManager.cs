﻿using UnityEngine;

public class FieldInputManager : MonoBehaviour
{
    InputController inputController;

	void Start()
    {
        inputController = GameObject.FindObjectOfType<InputController>() as InputController;

        if (inputController != null)
        {
            inputController.OnLeftClick += HandleLeftClick;
            inputController.OnRightClick += HandleRightClick;
         }
	}

    void HandleLeftClick(Vector3 clickPos)
    {
        Field.Instance.SetPathPoint(clickPos);
    }

    void HandleRightClick(Vector3 clickPos, bool isDragging)
    {
        Field.Instance.AddObstacle(clickPos, isDragging);
    }

    void OnDestroy()
    {
        inputController.OnLeftClick -= HandleLeftClick;
        inputController.OnRightClick -= HandleRightClick;
    }
}
